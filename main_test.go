package doublestar_windows_bug

import (
	"runtime"
	"testing"

	doublestarv3 "github.com/bmatcuk/doublestar/v3"
	doublestarv4 "github.com/bmatcuk/doublestar/v4"
	"github.com/stretchr/testify/assert"
)

func TestDoublestarWindows(t *testing.T) {
	if runtime.GOOS != "windows" {
		t.Skip()
	}

	pattern := "foo/**/*.md"
	path := "foo\\test\\bar\\baz\\1.md"

	match, _ := doublestarv3.PathMatch(pattern, path)
	assert.True(t, match)

	match, _ = doublestarv4.PathMatch(pattern, path)
	assert.False(t, match)
}

func TestDoublestarMacOS(t *testing.T) {
	if runtime.GOOS != "darwin" {
		t.Skip()
	}

	pattern := "foo/**/*.md"
	path := "foo/test/bar/baz/1.md"

	match, _ := doublestarv3.PathMatch(pattern, path)
	assert.True(t, match)

	match, _ = doublestarv4.PathMatch(pattern, path)
	assert.True(t, match)
}
